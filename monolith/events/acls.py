import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY

def get_photo(city, state):
    url = "https://api.pexels.com/v1/search"
    params = {"per_page": 1, "query": city + " " + state}
    headers = {"Authorization": PEXELS_API_KEY}
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)

    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}

def get_weather_data(city, state):
    url_geo = "http://api.openweathermap.org/geo/1.0/direct"
    params_geo = {
        "q": f"{city},{state},US",
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY
    }
    response = requests.get(url_geo, params=params_geo)
    content = json.loads(response.content)

    url_data = "https://api.openweathermap.org/data/2.5/weather"
    param_data = {
        "lat": content[0]["lat"],
        "lon": content[0]["lon"],
        "appid": OPEN_WEATHER_API_KEY,
        "lang": "en"
    }
    response_data = requests.get(url_data, params=param_data)
    content_data = json.loads(response_data.content)

    try:
        return {
                "temp": content_data["main"]["temp"],
                "description": content_data["weather"][0][
                    "description"
                ],
        }
    except (KeyError):
        return {
            "weather": None
        }
